﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sql表结构对比.IRepository
{
    //
    //负责专用功能界面的所有服务
    //
    interface IDedicatedService
    {
        //获取数据库表格，绑定到combBox里；
        List<string> BindCombBox(string wx_table);
        
    }
}
