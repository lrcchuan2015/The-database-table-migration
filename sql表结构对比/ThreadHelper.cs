﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using sql表结构对比.IRepository;
using sql表结构对比.ViewModel;
using sql表结构对比.DataBusinessLayer;
using sql表结构对比.Repository;
using System.Data.Entity;
using System.Data.Entity.Validation;
namespace sql表结构对比
{
    partial class Form1
    {
        private delegate void done(int pos);
        //点击测试连接时，进度条的显示
        #region
        private void SetProgressbar_Test()
        {
            for (int i = 20; i <= 100; i += 20)
            {
                Thread.Sleep(700);
                show_Test(i);
            }
        }
        private void show_Test(int pos)
        {
            if (this.InvokeRequired)
            {
                done Done = new done(show_Test);
                this.Invoke(Done, pos);
            }
            else
            {
                this.stateProgressbar.Value = pos;
                if (this.stateProgressbar.Value == 100)
                {
                    this.richtxtStateWorklog.Text = "连接成功！";
                    this.stateLabel.Text = "连接成功！";
                }
            }
        }
        #endregion

        //通用功能点击执行按钮，进度条的显示
        #region
        private void setProgress_Submit()
        {
            for (int i = 20; i <= 100; i += 20)
            {
                Thread.Sleep(700);
                show_submit(i);
            }
        }
        private void show_submit(int pos)
        {
            if (this.InvokeRequired)
            {
                done Done = new done(show_submit);
                this.Invoke(Done, pos);
            }
            else
            {
                this.stateProgressbar.Value = pos;
                if (this.stateProgressbar.Value == 100)
                {
                    this.txtErrorWorklog.Text = "执行成功!";
                    this.stateLabel.Text = "执行完毕！";
                }
            }
        }
        #endregion

        //单击一键导入时，进度条的显示
        private void timer1_Tick(object sender, EventArgs e)
        {
            stateProgressbar.Maximum = Common.allCount;
            if (stateProgressbar.Maximum != 0)
            {
                stateProgressbar.Value = Common.curProgress;
                if (stateProgressbar.Value == stateProgressbar.Maximum)
                {
                    timer1.Enabled = false;
                    stateLabel.Text = "执行完毕";
                }
            }
        }

        //获取数据库下的所有表名
        #region
        delegate void showComBData();
        showComBData swcb;
        List<string> StableNames = new List<string>();
        List<string> DtableNames = new List<string>();
        private void show_S()
        {
            combSourceTbs.DataSource = StableNames;
        }
        public void GetTableNames_S()
        {
            IUniversalService IUSer = new UniversalService();
            StableNames = IUSer.BindCombBox(SqlHelper.constrSocurce);
            this.BeginInvoke(swcb);
        }
        private void show_D()
        {
            combDesTbs.DataSource = DtableNames;
        }
        public void GetTableNames_D()
        {
            IUniversalService IUSer = new UniversalService();
            DtableNames = IUSer.BindCombBox(SqlHelper.constrDest);
            this.BeginInvoke(swcb);
        }
        #endregion

        //加载组织代码数据
        #region
        delegate void D_showComb(List<string> str);
        D_showComb D_show;
        private void D_showMethod(List<string> str)
        {
            comboBox3.DataSource = str;
        }
        List<string> str = new List<string>();
        public void ThreadMethod()
        {
            IDedicatedService iser = new DedicatedService();
            this.BeginInvoke(D_show, iser.BindCombBox(tables));
        }
        string tables = string.Empty;
        #endregion
        //数据一键导入
        #region
        delegate void D_show_workloginfo();
        D_show_workloginfo D_show_info;
        string erroinfo = string.Empty;
        string wx_tb = string.Empty;
        string OrgId = string.Empty;
        string option = "";
        List<string> basicinfo = new List<string>();
        void D_show_worklog()
        {
           
            if (erroinfo.Contains("将截断字符串或二进制数据"))
            {
                erroinfo += "\r\n" + "请检查源表中需要导入的字段的长度是否与对应目的表字段长度匹配，若不匹配，请更正后再试.";
                txtErrorWorkloginfo1.Text = erroinfo;
            }
            else
            {
                txtErrorWorkloginfo1.Text = erroinfo;
            }
            //int start = erroinfo.IndexOf("成");
            //int end = erroinfo.IndexOf("条");
            //if (erroinfo.Contains("全部"))
            //{
            //    txtErrorWorkloginfo.Select(0, erroinfo.Length);
            //    txtErrorWorkloginfo.SelectionColor = Color.Blue;
            //}
            //else
            //{
            //    txtErrorWorkloginfo.Select(start - 3, end);
            //    txtErrorWorkloginfo.SelectionColor = Color.Blue;
            //}
        }
        void threadstart_Dimport()
        {
            DedicatedService Dser = new DedicatedService();
            Dser.OneClickImport(basicinfo, out erroinfo,Dser.contorloOptional(option));

            this.BeginInvoke(D_show_info);
        }
        #endregion
        //绑定两张表的关系
        private void timer2_Tick(object sender, EventArgs e)
        {
            if (GetCheckedItemsText(checkedListBox1).Count != 0 && GetCheckedItemsText(checkedListBox3).Count != 0)
            {
                string temp = checkedListBox1.SelectedItem + "->" + checkedListBox3.SelectedItem;
                if (checkedListBox2.Items.Contains(temp)) 
                {

                }
                else
                {
                    checkedListBox2.Items.Add(temp);
                }
              
                //checkedListBox1.Items.Remove(checkedListBox1.SelectedItem);
                //checkedListBox3.Items.Remove(checkedListBox3.SelectedItem);
                timer2.Enabled = false;
            }
        }
    }
}
