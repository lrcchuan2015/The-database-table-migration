﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Data.SqlClient;
using sql表结构对比.ViewModel;
using sql表结构对比.IRepository;
namespace sql表结构对比
{
  partial  class Form1
    {
      //获取数据库链接字符串信息
      private string[] GetConnstrInfo()
      {
          string[] constr = new string[5];
          constr[0] = txtServerIP.Text;
          constr[1] = txtloginName.Text;
          constr[2] = txtLoginPwd.Text;
          constr[3] = combSourceSql.Text;
          constr[4] = comDestinationSql.Text;
          return constr;
      }

      //进度条初始化
        private void initialState()
        {
            this.stateLabel.Text = "就绪";
            this.stateProgressbar.Value = 0;
            stateProgressbar.Maximum = 100;
        }
   
      //测试连接
        private bool TestLink() {
            string stateinfo = "";
            if (combSourceSql.Text == "" || comDestinationSql.Text == "") {
                richtxtStateWorklog.Text = "连接失败！数据库名不能为空。请选择数据库名。";
                return false;
            }
            else
            {
                if (SqlHelper.Connection(GetConnstrInfo(), out stateinfo))
                {
                    //Thread thread1 = new Thread(new ThreadStart(SetProgressbar_Test));
                    //thread1.IsBackground = true;
                    //thread1.Start();
                    richtxtStateWorklog.Text = "连接成功！";
                    return true;
                }
                else
                {
                    richtxtStateWorklog.Text = stateinfo;
                    stateLabel.Text = "连接失败!";
                    return false;
                }
            }
           
        }

      //执行按钮点击功能
        private void Submit() {
            txtErrorWorklog.Text = string.Empty;
            string cmd = txtSqlCommand.Text.Trim();
            string errorinfo = string.Empty;
            if(SqlHelper.Connection(GetConnstrInfo(),out erroinfo)){
                 SqlHelper.ExecuteNonQuery(cmd, SqlHelper.constrDest, out errorinfo);
            if (errorinfo == "已成功执行！")
            {
                Thread thread2 = new Thread(new ThreadStart(setProgress_Submit));
                thread2.IsBackground = true;
                thread2.Start();
            }
            else
            {
                txtErrorWorklog.Text = errorinfo;
                this.stateLabel.Text = "执行失败！";
            }
            }
            else
            {
                txtErrorWorklog.Text = erroinfo;
            }
           
        }

      //数据库表名的加载
        #region
      //获取源数据表名
      private void GetSourTables(ComboBox T){
          string stateinfo = "";
          if (SqlHelper.Connection(GetConnstrInfo(), out stateinfo))
          {
              if (this.txtErrorWorklog.Text != "")
              {
                  this.txtErrorWorklog.Text = string.Empty;
              }
              this.btnSubmit.Enabled = true;
              contextMenuStrip2.Enabled = true;
              swcb = new showComBData(show_S);
              Thread thread = new Thread(new ThreadStart(GetTableNames_S));
              thread.IsBackground = true;
              thread.Start();
          }
          else
          {
             this.txtErrorWorkloginfo1.Text=this.txtErrorWorklog.Text = stateinfo + "\n" + "请进行服务器的登录。否则无法进行下一步操作！";
              contextMenuStrip2.Enabled = false;
             
              T.DataSource = null;
              T.Items.Clear();
              this.btnSubmit.Enabled = false;
          }
      }
      //获取目的数据库表名
      private void GetDesTables() {
          string stateinfo = "";
          if (SqlHelper.Connection(GetConnstrInfo(), out stateinfo))
          {
              if (this.txtErrorWorklog.Text != "")
              {
                  this.txtErrorWorklog.Text = string.Empty;
              }
              this.btnSubmit.Enabled = true;
           
              swcb = new showComBData(show_D);
              Thread thread = new Thread(new ThreadStart(GetTableNames_D));
              thread.IsBackground = true;
              thread.Start();
          }
          else
          {
              this.txtErrorWorklog.Text = stateinfo + "\n" + "请进行服务器的登录。否则无法进行下一步操作！";
              combDesTbs.DataSource = null;
              combDesTbs.Items.Clear();
              this.btnSubmit.Enabled = false;
          }
      }
        #endregion
      
    }
}
