﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Data.SqlClient;
using System.Collections;
using sql表结构对比.DataBusinessLayer;
using sql表结构对比.Model;
namespace sql表结构对比
{
    partial class Form1
    {

        //存放基本信息
        public List<string> GetBasicInfo() {
            List<string> temp = new List<string>();
            temp.Add(combSourceSql.Text);
            temp.Add(comDestinationSql.Text);
            return temp;
        }

        //获取指定表的列名
        public void GetColumnNames(string tablename, string constr, CheckedListBox T)
        {
            string cmd = "SELECT COLUMN_NAME  FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='" + tablename + "'";
            SqlDataReader reader = SqlHelper.GetReader(cmd, constr);
            List<string> ColumnNames = new List<string>();
            while (reader.Read())
            {
                string Columnname = reader["COLUMN_NAME"].ToString();
                T.Items.Add(Columnname);

            }
        }
      //已存在的关系要显示在关系列表中
        public void showRelationship(List<string> basicinfo) 
        {
            DedicatedService ds = new DedicatedService();
            tablerelationship temp = ds.comfirm(basicinfo);
            if (temp == null) {
                checkedListBox2.Items.Clear();
            }
            else
            {
                foreach (string item in temp.filedRelationship)
                {
                    checkedListBox2.Items.Add(item);
                }
            }
        }

        //确保checklistbox每次只能选中一个
        public void selectJustOne(object sender, ItemCheckEventArgs e, CheckedListBox T)
        {
            if (T.CheckedItems.Count > 0)
            {
                for (int i = 0; i < T.Items.Count; i++)
                {
                    if (i != e.Index)
                    {
                        T.SetItemCheckState(i, System.Windows.Forms.CheckState.Unchecked);
                    }
                }
            }
        }

        //获取checklistbox选中的值
        private List<string> GetCheckedItemsText(CheckedListBox clb)
        {
            List<string> result = new List<string>();
            IEnumerator myEnumerator = clb.CheckedIndices.GetEnumerator();
            int index;
            while (myEnumerator.MoveNext())
            {
                index = (int)myEnumerator.Current;
                clb.SelectedItem = clb.Items[index];
                result.Add(clb.Text);
            }
            return result;
        }
        //获取checklistbox的所有值
        public List<string> GetAllvalue(CheckedListBox T) {
            List<string> Temp = new List<string>();
            foreach (var item in T.Items)
            {
                Temp.Add(item.ToString());
            }
            return Temp;
        }

        //全选
        public bool SelectALL() {
            for (int i = 0; i < this.checkedListBox2.Items.Count; i++)
            {
                this.checkedListBox2.SetItemChecked(i, true);
            }
            return false;
        }
        //取消全选
        public bool CancelALL() {
            for (int i = 0; i < checkedListBox2.Items.Count; i++)
            {
                if (checkedListBox2.GetItemChecked(i))
                {
                    checkedListBox2.SetItemChecked(i, false);
                }
                else
                {
                    checkedListBox2.SetItemChecked(i, true);
                }
            }
            return true;
        }



        //错误补充
        //public bool error1(DataTable dt_S, DataTable dt_D)
        //{
        //    if (dt_S != null && dt_D != null) {
        //        dataGridView1.DataSource = dt_S;
        //        dataGridView2.DataSource = dt_D;
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
       

        //}
    }
}
