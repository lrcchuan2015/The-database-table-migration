﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using sql表结构对比.Model;
using sql表结构对比.Repository;
using System.Data.Entity.Validation;
using sql表结构对比.IRepository;
namespace sql表结构对比.ViewModel
{
    class UniversalService : IUniversalService
    {
       
        //为combBox绑定数据库下的表名信息
        public List<string> BindCombBox(string connstr)
        {
            string cmd = "select name from sys.tables go";
            List<string> TableNames = new List<string>();
            SqlDataReader reader = SqlHelper.GetReader(cmd, connstr);
            while (reader.Read())
            {
                string TableName = string.Empty;
                TableName = reader["name"].ToString();
                TableNames.Add(TableName);
            }
            return TableNames;
        }
       
       

    }
}
