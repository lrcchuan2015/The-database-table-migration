﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
namespace sql表结构对比
{
    class tablerelationship
    {
        public List<string> basicinfo = new List<string>();
        public List<string> filedRelationship = new List<string>();
    }
    class xmlOperate
    {
        //建立表关系。写入xml文件
        public void AddTableRelationship(List<string> T, List<string> basicinfo)
        {
            XmlDocument xml = new XmlDocument();
            XmlNode root;
            if (File.Exists("tableRelationship.xml"))
            {
                xml.Load("tableRelationship.xml");
            }
            root = xml.SelectSingleNode("Relationship");
            //XmlElement root=  xml.CreateElement("Relationship");
            //xml.AppendChild(root);
            XmlElement tableRelationship = xml.CreateElement("tableRelationship");
            root.AppendChild(tableRelationship);
            XmlElement database = xml.CreateElement("basicInfo");
            tableRelationship.AppendChild(database);
            XmlElement sourDB = xml.CreateElement("sourDB");
            sourDB.InnerText = basicinfo[0];
            database.AppendChild(sourDB);
            XmlElement DesDB = xml.CreateElement("DesDB");
            DesDB.InnerText = basicinfo[1];
            database.AppendChild(DesDB);
            XmlElement SourTb = xml.CreateElement("sourTb");
            SourTb.InnerText = basicinfo[2];
            database.AppendChild(SourTb);
            XmlElement DesTb = xml.CreateElement("DesTb");
            DesTb.InnerText = basicinfo[3];
            database.AppendChild(DesTb);
            XmlElement fieldRelationship = xml.CreateElement("fieldRelationship");
            tableRelationship.AppendChild(fieldRelationship);
            foreach (var item in T)
            {
                XmlElement fieldName = xml.CreateElement("fieldName");
                fieldName.InnerText = item;
                fieldRelationship.AppendChild(fieldName);
            }
            xml.Save(@"tableRelationship.xml");
        }

        //读取xml文件，获取表关系
        public List<tablerelationship> GetTableRelationship()
        {
            List<tablerelationship> list = new List<tablerelationship>();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load("tableRelationship.xml"); //加载xml文件
            XmlNode xn = xmlDoc.SelectSingleNode("Relationship");
            XmlNodeList xnl = xn.ChildNodes;//所有的tableRelationship
            foreach (XmlNode xnf in xnl)
            {
                XmlElement xe = (XmlElement)xnf;
                XmlNodeList xnf1 = xe.ChildNodes;//database 和 fieldRelationship
                int temp = 0;
                tablerelationship tl = new tablerelationship();
                foreach (XmlNode xn2 in xnf1)
                {
                    //Console.WriteLine(xn2.InnerText);//显示子节点点文本 
                    temp++;
                    XmlElement xnf2_1 = (XmlElement)xn2;
                    if (temp == 1)
                    {
                        foreach (XmlNode xn3 in xnf2_1)
                        {
                            tl.basicinfo.Add(xn3.InnerText);
                        }
                    }
                    if (temp == 2)
                    {
                        foreach (XmlNode xn3 in xnf2_1)
                        {
                            tl.filedRelationship.Add(xn3.InnerText);
                        }
                    }

                }
                list.Add(tl);
            }
            return list;
        }

        //修改表关系，写入xml文件
        public void UpdataTableRelationship(List<string> basicinfo, List<string> relation)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(@"tableRelationship.xml"); //加载xml文件
            //获取bookshop节点的所有子节点 
            XmlNodeList nodeList = xmlDoc.SelectSingleNode("Relationship").ChildNodes;//获取所有的关系表

            //遍历所有子节点 
            foreach (XmlNode xn in nodeList)
            {
                XmlElement xe = (XmlElement)xn; //将子节点类型转换为XmlElement类型 
                XmlNodeList nls = xe.ChildNodes;//得到一张表里面的子节点（basicinfo节点和fieldRelationship节点）
                int temp = 0;
                bool f = false;
                foreach (XmlNode xn1 in nls)//遍历 
                {
                    temp++;
                    bool flag = false;

                    XmlElement xe2 = (XmlElement)xn1; //转换类型
                    if (temp % 2 == 1)
                    {
                        //匹配信息,若不是我要修改的表关系就continue
                        int x = 0;

                        foreach (XmlNode xn3 in xe2)
                        {
                            if (basicinfo[x] == xn3.InnerText)
                            {
                                x++;
                            }
                            else
                            {
                                flag = true;
                                break;
                            }
                            if (x == 4)
                            {
                                f = true;
                                flag = true;
                            }
                        }
                        if (flag == true) { continue; }
                    }

                    if (f)
                    {
                        xe2.RemoveAll();
                        if (relation.Count == 0)
                        {
                            //删除一个表关系节点
                            XmlNode xx = xe2.ParentNode;
                            xx.ParentNode.RemoveChild(xx);
                        }
                        else
                        {
                            foreach (var item in relation)
                            {
                                XmlElement fieldName = xmlDoc.CreateElement("fieldName");
                                fieldName.InnerText = item;
                                xe2.AppendChild(fieldName);
                            }
                        }
                        goto less;
                    }
                }
            }

        less: xmlDoc.Save(@"tableRelationship.xml");//保存。 
        }
    }
}
